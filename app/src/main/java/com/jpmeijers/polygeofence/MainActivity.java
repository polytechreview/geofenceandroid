package com.jpmeijers.polygeofence;

import android.Manifest;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, ResultCallback<Status>, OnMapReadyCallback {

    private static String TAG = "Main Activity";
    private OkHttpClient httpClient = new OkHttpClient();

    private LocationRequest mLocationRequest;

    private Location lastGeofenceDownloadLocation;
    private List<Geofence> mGeofenceList = new ArrayList<>();

    private PendingIntent mGeofencePendingIntent;

    private GoogleMap mMap;

    // Broadcast receiver for intents sent by services
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String message = intent.getStringExtra("message");
            String payloadData = intent.getStringExtra("payload");
            Log.d(TAG, "Got message: " + message);

            switch (message) {
                case "notification":
                    setStatusMessage(payloadData);
                    break;
                default:
                    Log.d(TAG, "Unknown message received");
            }
        }
    };

    Call postToServer(String url, String json, Callback callback) throws IOException {
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Call call = httpClient.newCall(request);
        call.enqueue(callback);
        return call;
    }

    Call getFromServer(HttpUrl url, Callback callback) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Call call = httpClient.newCall(request);
        call.enqueue(callback);
        return call;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, "onCreate");

        mGeofencePendingIntent = null;

        startGooglePlayServices();


        // hide top bar - if exist
        try {
            getSupportActionBar().hide();
        } catch (NullPointerException e) {
            // the action bar does not exist, ignore
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        MyApplication mApplication = (MyApplication) getApplicationContext();
        if (mApplication.getmGoogleApiClient() != null) {
            mApplication.getmGoogleApiClient().connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        MyApplication mApplication = (MyApplication) getApplicationContext();
        if (mApplication.getmGoogleApiClient() != null) {
            mApplication.getmGoogleApiClient().disconnect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // keep the screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Register mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("polygeofence-notifications"));
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    protected synchronized void startGooglePlayServices()
    {
        MyApplication mApplication = (MyApplication) getApplicationContext();

        // Create an instance of GoogleAPIClient.
        if (mApplication.getmGoogleApiClient() == null) {
            mApplication.setmGoogleApiClient(new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(ActivityRecognition.API)
                    .build());
        }

        mApplication.getmGoogleApiClient().connect();
    }

    private void downloadClosestThreeGeofences(double lat, double lon)
    {

        HttpUrl url = new HttpUrl.Builder()
                .scheme("https")
                .host(getString(R.string.api_host))
                .addPathSegment(getString(R.string.api_endpoint))
                .addQueryParameter("lat", String.valueOf(lat))
                .addQueryParameter("lon", String.valueOf(lon))
                .build();

        try {
            getFromServer(url, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.d(TAG, "Error uploading");
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        final String returnedString = response.body().string();
                        Log.d(TAG, "HTTP response: " + returnedString);

                        MyApplication mApplication = (MyApplication)getApplicationContext();

                        try
                        {
                            JSONObject jObject = new JSONObject(returnedString);
                            JSONArray fences = jObject.getJSONArray("fences");

                            for(int i=0; i<fences.length(); i++)
                            {
                                JSONObject newFence = fences.getJSONObject(i);
                                MyGeofence newMyGeofence = new MyGeofence();
                                newMyGeofence.setName(newFence.getString("name"));
                                newMyGeofence.setLat(newFence.getDouble("lat"));
                                newMyGeofence.setLon(newFence.getDouble("lon"));
                                newMyGeofence.setRange(newFence.getDouble("range"));

                                Log.d(TAG, "New geofence:\n"+newMyGeofence);

                                if(!mApplication.getGeofences().containsKey(newMyGeofence.getName()))
                                {
                                    mApplication.addGeofence(newMyGeofence);
                                    addNewGoogleGeofence(newMyGeofence);
                                }
                            }
                            removeGeofences();
                            registerGeofenceList();

                            runOnUiThread(new Runnable() {
                                public void run()
                                {
                                    clearAndReaddAllToMap();
                                }
                            });

                            Log.d(TAG, "Total number of known geofences: "+mApplication.getGeofences().size());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        // Request not successful
                        Log.d(TAG, "server error");

                    }
                }
            });
        } catch (IOException e) {
            Log.d(TAG, "HTTP call IO exception");
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "Google API client connected");

        MyApplication mApplication = (MyApplication) getApplicationContext();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(500);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // We should have permission as we ask for it at startup.
            Log.e(TAG, "We do not have permission to access location information");
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mApplication.getmGoogleApiClient(), mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Google API client connect failed");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChange");

        if(lastGeofenceDownloadLocation==null)
        {
            lastGeofenceDownloadLocation=location;
            downloadClosestThreeGeofences(location.getLatitude(), location.getLongitude());
        }
        else if(location.distanceTo(lastGeofenceDownloadLocation)>1000) //distanceTo returns a value in meters
        {
            //when we moved more than a kilometer, refresh our list of geofences
            lastGeofenceDownloadLocation=location;
            downloadClosestThreeGeofences(location.getLatitude(), location.getLongitude());
        }

    }

    private void addNewGoogleGeofence(MyGeofence myGeofence)
    {
        Log.d(TAG, "Creating new geofence with builder: "+myGeofence.getName());
        mGeofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this
                // geofence.
                .setRequestId(myGeofence.getName())

                .setCircularRegion(
                        myGeofence.getLat(),
                        myGeofence.getLon(),
                        (float)myGeofence.getRange()
                )
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());
    }

    private void registerGeofenceList()
    {
        Log.d(TAG, "Registering list of geofences with location api");


        MyApplication mApplication = (MyApplication) getApplicationContext();

        if (!mApplication.getmGoogleApiClient().isConnected()) {
            Toast.makeText(this, "Google api client not connected", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            LocationServices.GeofencingApi.addGeofences(
                    mApplication.getmGoogleApiClient(),
                    // The GeofenceRequest object.
                    getGeofencingRequest(),
                    // A pending intent that that is reused when calling removeGeofences(). This
                    // pending intent is used to generate an intent when a matched geofence
                    // transition is observed.
                    getGeofencePendingIntent()
            ).setResultCallback(this); // Result processed in onResult().
        } catch (SecurityException securityException) {
            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
            Log.d(TAG, "Error while registering geofence list");
        }
    }

    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(mGeofenceList);

        // Return a GeofencingRequest.
        return builder.build();
    }

    /**
     * Removes geofences, which stops further notifications when the device enters or exits
     * previously registered geofences.
     */
    public void removeGeofences() {

        MyApplication mApplication = (MyApplication) getApplicationContext();

        if (!mApplication.getmGoogleApiClient().isConnected()) {
            Toast.makeText(this, "Google api not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            // Remove geofences.
            LocationServices.GeofencingApi.removeGeofences(
                    mApplication.getmGoogleApiClient(),
                    // This is the same pending intent that was used in addGeofences().
                    getGeofencePendingIntent()
            ).setResultCallback(this); // Result processed in onResult().
        } catch (SecurityException securityException) {
            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
            Log.d(TAG, "No permission to access location");
        }
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        return PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
    }

    /**
     * Runs when the result of calling addGeofences() and removeGeofences() becomes available.
     * Either method can complete successfully or with an error.
     *
     * Since this activity implements the {@link ResultCallback} interface, we are required to
     * define this method.
     *
     * @param status The Status returned through a PendingIntent when addGeofences() or
     *               removeGeofences() get called.
     */
    public void onResult(@NonNull Status status) {
        if (status.isSuccess()) {
            Log.d(TAG, "Geofences were added or removed");
        } else {
            // Get the status code for the error and log it using a user-friendly message.
            String errorMessage = GeofenceErrorMessages.getErrorString(this,
                    status.getStatusCode());
            Log.e(TAG, errorMessage);

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "No permission to access location");
            } else {
                mMap.setMyLocationEnabled(true);
            }

            //Hide navigation buttons
            mMap.getUiSettings().setMapToolbarEnabled(false);
            mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
            mMap.getUiSettings().setTiltGesturesEnabled(false);
            mMap.getUiSettings().setRotateGesturesEnabled(false);

            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.setPadding(0, 0, 0, 0);

            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.map_style));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }

            clearAndReaddAllToMap();
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
    }

    private void clearAndReaddAllToMap()
    {
        if(mMap==null)
        {
            return;
        }
        mMap.clear();

        MyApplication mApplication = (MyApplication)getApplicationContext();

        for (MyGeofence myGeofence: mApplication.getGeofences().values())
        {
            addGeofenceToMap(myGeofence);
        }
    }

    private void addGeofenceToMap(MyGeofence myGeofence)
    {
        mMap.addCircle(new CircleOptions()
                .center(new LatLng(myGeofence.getLat(), myGeofence.getLon()))
                .radius(myGeofence.getRange())
                .strokeWidth(1)
                .strokeColor(Color.parseColor("#AA0000FF"))
                .fillColor(Color.parseColor("#440000FF")));
    }

    public void setStatusMessage(String message) {
        TextView tv = (TextView) findViewById(R.id.textViewStatus);
        tv.setText(message);
    }
}
