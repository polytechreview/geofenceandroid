package com.jpmeijers.polygeofence;

/**
 * Author: JP Meijers
 * 2017-03-07
 */

public class MyGeofence {
    private String name;
    private double lat;
    private double lon;
    private double range;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getRange() {
        return range;
    }

    public void setRange(double range) {
        this.range = range;
    }

    public String toString()
    {
        return "Name="+name+" Lat="+lat+" Lon="+lon+" Radius="+range;
    }
}
