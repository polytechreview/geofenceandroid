package com.jpmeijers.polygeofence;


import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;

/**
 * Author: JP Meijers
 * 2017-03-07
 */

public class ActivityRecognitionService extends IntentService {

    private static String TAG = "ActivityRecognition";

    public ActivityRecognitionService() {
        super("ActivityRecognitionService");
    }

    public ActivityRecognitionService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        MyApplication mApplication = (MyApplication) getApplicationContext();
        if (mApplication.isInsideGeofence() == false) {
            //If we are not inside a geofence, do not do activity recognition.
            //This is a hack for now. We should disable activity updates from the google api services.
            return;
        }
        if (ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            handleDetectedActivities(result.getProbableActivities());
        }
    }

    private void handleDetectedActivities(List<DetectedActivity> probableActivities) {
        for (DetectedActivity activity : probableActivities) {
            switch (activity.getType()) {
                case DetectedActivity.IN_VEHICLE: {
                    Log.d(TAG, "In Vehicle: " + activity.getConfidence());
                    if (activity.getConfidence() >= 75) {
                        sendNotification("You are in a vehicle");
                    }
                    break;
                }
                case DetectedActivity.ON_BICYCLE: {
                    Log.d(TAG, "On Bicycle: " + activity.getConfidence());
                    if (activity.getConfidence() >= 75) {
                        sendNotification("You are cycling");
                    }
                    break;
                }
                case DetectedActivity.ON_FOOT: {
                    Log.d(TAG, "On Foot: " + activity.getConfidence());
                    if (activity.getConfidence() >= 75) {
                        sendNotification("You are on foot");
                    }
                    break;
                }
                case DetectedActivity.RUNNING: {
                    Log.d(TAG, "Running: " + activity.getConfidence());
                    if (activity.getConfidence() >= 75) {
                        sendNotification("You are running");
                    }
                    break;
                }
                case DetectedActivity.STILL: {
                    Log.d(TAG, "Still: " + activity.getConfidence());
                    if (activity.getConfidence() >= 75) {
                        sendNotification("The phone is likely laying on your desk.");
                    }
                    break;
                }
                case DetectedActivity.TILTING: {
                    Log.d(TAG, "Tilting: " + activity.getConfidence());
                    if (activity.getConfidence() >= 75) {
                        sendNotification("Tilting!");
                    }
                    break;
                }
                case DetectedActivity.WALKING: {
                    Log.d(TAG, "Walking: " + activity.getConfidence());
                    if (activity.getConfidence() >= 75) {
                        sendNotification("You are walking");
                    }
                    break;
                }
                case DetectedActivity.UNKNOWN: {
                    Log.d(TAG, "Unknown: " + activity.getConfidence());
                    if (activity.getConfidence() >= 75) {
                        sendNotification("You are doing an unknown activity");
                    }
                    break;
                }
            }
        }
    }


    private void sendNotification(String message) {
        Intent intent = new Intent("polygeofence-notifications");
        // add data
        intent.putExtra("message", "notification");
        intent.putExtra("payload", message);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
