package com.jpmeijers.polygeofence;

import android.app.Application;
import android.content.res.Configuration;

import com.google.android.gms.common.api.GoogleApiClient;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: JP Meijers
 * 2017-03-07
 */

public class MyApplication extends Application {
    private static final String TAG = "MyApplication";
    private static MyApplication singleton;

    private double myLat = 0;
    private double myLon = 0;
    private boolean isInsideGeofence = false;
    private Map<String, MyGeofence> geofences = new HashMap<>();

    private GoogleApiClient mGoogleApiClient;

    public static MyApplication getInstance() {
        return singleton;
    }

    public boolean isInsideGeofence() {
        return isInsideGeofence;
    }

    public void setInsideGeofence(boolean insideGeofence) {
        isInsideGeofence = insideGeofence;
    }

    public double getMyLat() {
        return myLat;
    }

    public void setMyLat(double myLat) {
        this.myLat = myLat;
    }

    public double getMyLon() {
        return myLon;
    }

    public void setMyLon(double myLon) {
        this.myLon = myLon;
    }

    public Map<String, MyGeofence> getGeofences() {
        return geofences;
    }

    public void setGeofences(Map<String, MyGeofence> geofences) {
        this.geofences = geofences;
    }

    public void addGeofence(MyGeofence newMyGeofence)
    {
        if(!geofences.containsKey(newMyGeofence.getName()))
        {
            geofences.put(newMyGeofence.getName(), newMyGeofence);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public GoogleApiClient getmGoogleApiClient() {
        return mGoogleApiClient;
    }

    public void setmGoogleApiClient(GoogleApiClient mGoogleApiClient) {
        this.mGoogleApiClient = mGoogleApiClient;
    }
}
